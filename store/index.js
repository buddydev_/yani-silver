export const state = () => ({
  products: [
    {
      img: 'https://www.zales.com/productimages/processed/V-20108948_0_565.jpg',
      price: '$29',
      code: 'RING123',
      category: 'ring'
    },
    {
      img: 'https://www.zales.com/productimages/processed/V-20108948_0_565.jpg',
      price: '$29',
      code: 'RING123',
      category: 'ring'
    },
    {
      img: 'https://i.ibb.co/X8TSGm5/bangle.png',
      price: '$50',
      code: 'BANGLE123',
      category: 'bangle'
    },
    {
      img: 'https://i.ibb.co/WFmFqdr/chain.png',
      price: '$30',
      code: 'CHAIN123',
      category: 'chain'
    },
    {
      img: 'https://i.ibb.co/0rHQbfc/pendant.png',
      price: '$35',
      code: 'PENDANT123',
      category: 'pendant'
    },
    {
      img: 'https://i.ibb.co/tbySn2M/other.jpg',
      price: '$100',
      code: 'OTHERS123',
      category: 'others'
    },
    {
      img: 'https://i.ibb.co/Y2DThQC/earring.png',
      price: '$100',
      code: 'EARRING123',
      category: 'earring'
    },
  ]
})

export const getters = {
  getProducts: (state) => (category) => {
    return state.products.filter(product => product.category === category)
  }
}
